import React from 'react';
import ReactDOM from 'react-dom';
import {Table, Column, Cell} from 'fixed-data-table';

import {CarTool} from './car_tool';
import {ColorTool} from './color_tool';

require('bootstrap-loader');

const colors = [ 'red', 'blue', 'white', 'black', 'yellow', 'green' ];
// const carsList = [
// 	{"id": 1, "make": "honda", "model": "accord", "year": 2010, "color": "white", price: 2000 },
// 	{"id": 2, "make": "toyota", "model": "camry", "year": 2010, "color": "white", price: 20000 },
// ];

class CarToolContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			cars: []
		};
	}

	componentDidMount() {
		fetch('http://localhost:5000/cars')
			.then(res => res.json())
			.then(carsList => { this.setState({ cars: carsList }) });
	}

	render() {
		return <CarTool carsList={this.state.cars} />;
	}
}

fetch('http://localhost:5000/cars')
		.then(res => res.json())
		.then(cars => ReactDOM.render(<CarTool carsList={cars} />, document.querySelector('main')));
		

// ReactDOM.render(<CarToolContainer />, document.querySelector('main'));


// ReactDOM.render(<CarTool carsList={carsList} />, document.querySelector('main'));

// ReactDOM.render(<ColorTool myColors={colors} />, document.querySelector('main'));
// import 'bootstrap-loader';
// import '../css/styles.scss';

// class HelloWorld extends React.Component {

//     render() {
//         return <h1>Hello World!</h1>;
//     }
// }

// ReactDOM.render( <HelloWorld /> , document.querySelector('main'));
