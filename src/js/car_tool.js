import React from 'react';
import {Header1, UnorderedList} from './html_elements';

class CarRow extends React.Component {
	static propTypes = {
		car: React.PropTypes.shape({ 
			id: React.PropTypes.number,
			make: React.PropTypes.string,
			model: React.PropTypes.string,
			year: React.PropTypes.number,
			color: React.PropTypes.string,
			price: React.PropTypes.number
		}),
		onDelete: React.PropTypes.func.isRequired,
		onEdit: React.PropTypes.func.isRequired,
		onCancel: React.PropTypes.func.isRequired,
		onSave: React.PropTypes.func.isRequired,
		editMode: React.PropTypes.bool,
	};

	constructor(props) {
		super(props);
		this.state = {
			editMake: this.props.car.make,
			editModel: this.props.car.model,
			editYear: this.props.car.year,
			editColor: this.props.car.color,
			editPrice: this.props.car.price
		};
	}

	onChange = (e) => {
		this.setState({
			[e.currentTarget.name]: e.currentTarget.value
		});
	}

	onSave = (e) => {
		let updateCar = {
			"id": parseInt(this.props.car.id),
			"make": this.state.editMake,
			"model": this.state.editModel,
			"year": parseInt(this.state.editYear),
			"color": this.state.editColor,
			"price": parseInt(this.state.editPrice)
		};
		this.props.onCancel();
		this.props.onSave(updateCar);
	}

	onEdit = () => {
		this.props.onEdit(this.props.car.id);
	}

	onDelete = () => {
		this.props.onDelete(this.props.car.id);
	}

	onCancel = () => {
		this.props.onCancel();
	}

	formatCurrency = (price) => {
		const dollars = String(parseInt(price / 100));
		let cents = String(parseInt(price % 100));
		if (cents.length == 1) {
			cents = cents + "0";
		}
		return "$" + dollars + "." + cents;
	}


	showEditRow = () => {
		return <tr>	
				<td><input type="text" id="edit-make-input" name="editMake" 
					value={this.state.editMake} className="form-control" onChange={this.onChange} />
				</td>
				<td><input type="text" id="edit-model-input" name="editModel" 
					value={this.state.editModel} className="form-control" onChange={this.onChange} />
				</td>
				<td><input type="text" id="edit-year-input" name="editYear" 
					value={this.state.editYear} className="form-control" onChange={this.onChange} />
				</td>
				<td><input type="text" id="edit-color-input" name="editColor" 
					value={this.state.editColor} className="form-control" onChange={this.onChange} />
				</td>
				<td><input type="text" id="edit-price-input" name="editPrice" 
					value={this.state.editPrice} className="form-control" onChange={this.onChange} />
				</td>
				<td>
					<button className="btn-primary" onClick={this.onSave}>Save</button>
					<button className="btn-warning" onClick={this.onCancel}>Cancel</button>
				</td>
			</tr>;
	}

	showViewRow = () => {
		return <tr>	
				<td>{this.props.car.make}</td>
				<td>{this.props.car.model}</td>
				<td>{this.props.car.year}</td>
				<td>{this.props.car.color}</td>
				<td>{this.formatCurrency(this.props.car.price)}</td>
				<td>
					<button className="btn-primary" onClick={this.onEdit}>Edit</button>&nbsp;
				    <button className="btn-danger" onClick={this.onDelete}>Delete</button>
			    </td>
			</tr>;		
	}

	render() {
		return (this.props.editMode ? this.showEditRow(): this.showViewRow());
	}
}


class CarTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			currentCarId: ''
		};
	}

	static propTypes = {
		carsList: React.PropTypes.array,
		onDelete: React.PropTypes.func.isRequired,
		onSave: React.PropTypes.func.isRequired
	};

	updateCarId = (carId) => {		
		this.setState({ currentCarId: String(carId) });
	}

	cancelEdit = () => {
		this.setState({ currentCarId: '' });
	}

	render() {
		return <table className="table table-responsive table-striped">
				<thead>
					<tr>
						<th>Make</th>
						<th>Model</th>
						<th>Year</th>
						<th>Color</th>
						<th>Price</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					{this.props.carsList.map(c => 
						<CarRow key={c.id} car={c} 
						onDelete={this.props.onDelete} 
						onEdit={this.updateCarId} 
						onCancel={this.cancelEdit}
						onSave={this.props.onSave}
						editMode={this.state.currentCarId === String(c.id)} />
					)}
				</tbody>
			</table>;
	}

}

class CarForm extends React.Component {
	static propTypes = {
		onSubmit: React.PropTypes.func.isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			newMake: '',
			newModel: '',
			newYear: '',
			newColor: '',
			newPrice: ''
		};
	}

	onChange = (e) => {
		this.setState({
			[e.currentTarget.name]: e.currentTarget.value
		});
	}

	onSubmit = (e) => {
		let newCar = {
			"id": parseInt(Math.floor(Math.random() * 10000)),
			"make": this.state.newMake,
			"model": this.state.newModel,
			"year": parseInt(this.state.newYear),
			"color": this.state.newColor,
			"price": parseInt(this.state.newPrice)
		};
		this.props.onSubmit(newCar);
		this.state = {
			newMake: '',
			newModel: '',
			newYear: '',
			newColor: '',
			newPrice: ''
		};	
	}

	render() {
		return <form>
				<div className="form-group">
					<label htmlFor="new-make-input">Make</label>
					<input type="text" id="new-make-input" name="newMake" 
						value={this.state.newMake} onChange={this.onChange} 
						className="form-control" />
				</div>
				<div className="form-group">
					<label htmlFor="new-model-input">Model</label>
					<input type="text" id="new-model-input" name="newModel" 
						value={this.state.newModel} onChange={this.onChange} 
						className="form-control" />
				</div>
				<div className="form-group">
					<label htmlFor="new-year-input">Year</label>
					<input type="text" id="new-year-input" name="newYear" 
						value={this.state.newYear} onChange={this.onChange} 
						className="form-control" />
				</div>
				<div className="form-group">
					<label htmlFor="new-color-input">Color</label>
					<input type="text" id="new-color-input" name="newColor" 
						value={this.state.newColor} onChange={this.onChange} 
						className="form-control" />
				</div>
				<div className="form-group">
					<label htmlFor="new-price-input">Price</label>
					<input type="text" id="new-price-input" name="newPrice" 
						value={this.state.newPrice} onChange={this.onChange} 
						className="form-control" />
				</div>

				<div className="form-group">
					<input type="button" value="Add New" onClick={this.onSubmit} 
					className="btn btn-success"/>
				</div>
			</form>
	}
}

export class CarTool extends React.Component {	

	constructor(props) {
		super(props);
		this.state = { 
			cars: this.props.carsList.concat(),
		};		
	}

	static propTypes = {
		carsList: React.PropTypes.array
	};		

	addCar = (newCar) => {
		this.setState({ cars: this.state.cars.concat(newCar) });
	}

	deleteCar = (carId) => {
		this.setState({ cars: this.state.cars.filter(c => c.id != carId)});
	}

	saveCar = (carUpdate) => {
		let newCars = this.state.cars.filter(c => c.id != carUpdate.id);
		newCars = newCars.concat(carUpdate);
		this.setState({ cars: newCars });
	}

	render() {
	    return <div className="container">
	    		<div className="row">
	    			<div className="col-md-12">
	    				<Header1 headerText="Cars List" />
	    			</div>
	    		</div>
	    		<div className="row">
			      <div className="col-md-9">
			      	<CarTable carsList={this.state.cars} 
			      		onDelete={this.deleteCar} 
			      		onSave={this.saveCar} />
		      	  </div>
			      <div className="col-md-3">
			      	<CarForm onSubmit={this.addCar} />
			      </div>
			    </div>
			   </div>;
	}

}