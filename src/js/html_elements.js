import React from 'react';

export class Header1 extends React.Component {
	static propTypes = {
		headerText: React.PropTypes.string
	};

	render() {
		return <h3>{this.props.headerText}</h3>;
	}
}

export class UnorderedList extends React.Component {
	static propTypes = {
		listItems: React.PropTypes.array
	};

	render() {
		return <ul>
			{this.props.listItems.map(listItem => 
				<li>{listItem}</li>)}
		</ul>;
	}
}