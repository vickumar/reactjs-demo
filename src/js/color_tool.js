import React from 'react';
import {Header1, UnorderedList} from './html_elements';

class ColorForm extends React.Component {
	static propTypes = {
		addColor: React.PropTypes.func.isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			newColor: ''
		};		
	}

	onChange = (e) => {
		this.setState({
			[e.currentTarget.name]: e.currentTarget.value
		});
	}		

	addNewColor = () => {
		this.props.addColor(this.state.newColor);
		this.setState({
			newColor: ''
		});
	}


	render() {
		return <form>
			<div>
				<label htmlFor="new-color-input">New Color</label>
				<input type="text" id="new-color-input" name="newColor" 
					value={this.state.newColor} onChange={this.onChange} />
			</div>
			<button type="button" onClick={this.addNewColor}>Add Color</button>
		</form>;
	}
}

class ColorTool extends React.Component {
	constructor(props) {
		super(props);
		this.state = { colors: this.props.myColors.concat() };
	}

	static propTypes = {
		myColors: React.PropTypes.array
	};	

	addColor = (newColor) => {
		this.setState({ colors: this.state.colors.concat(newColor) });
	}

	render() {
		return <div>
			<Header1 headerText="Car Tool" />
			<UnorderedList listItems={this.state.colors} />
			<ColorForm addColor={this.addColor} />
		</div>;
	}

}